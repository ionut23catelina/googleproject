from datetime import datetime

from django.shortcuts import render
# Create your views here.
from django.template import RequestContext
from django.utils.dateparse import parse_date
from django.views.decorators.csrf import csrf_protect, csrf_exempt

from games.models import *


class UserOrder:
    shoppingCart = []
    logged_user = ""
    games_query = None


order = UserOrder()
game_to_view_id = 0


def game_details(request):
    game_to_view_id = request.POST['id']
    my_game = Game.objects.raw("select * from games_game where id=" + game_to_view_id)
    my_review = Review.objects.raw("select * from games_review where game_id=" + game_to_view_id)
    return render(request, 'game_details.html', {'game': my_game}, {'reviews': my_review})


def add_review(request):
    game = Game.objects.get(id=game_to_view_id)
    user = User.objects.get(username=order.logged_user)
    review_comment = request.POST.get("comment")
    review = Review(comment=review_comment, game=game, user=user)
    review.save()
    return render(request, "user_home_page.html")


def admin_or_user(request):
    if order.logged_user == "":
        return render(request, "admin_or_user.html")

    return render(request, "admin_or_user.html")


def home_page(request):
    order.logged_user = "admin"

    return render(request, "home_page.html")


def index(request):
    if order.logged_user == "":
        return render(request, "admin_or_user.html")

    all_games = Game.objects.raw("select * from games_game")
    return render(request, 'index.html', {'games': all_games})


@csrf_exempt
def add_to_cart(request):
    if order.logged_user == "":
        return render(request, "admin_or_user.html")

    game_name = request.POST['name']
    order.shoppingCart.append(game_name)
    return render(request, 'index.html')


@csrf_exempt
def shopping_cart(request):
    if order.logged_user == "":
        return render(request, "admin_or_user.html")

    games = "("
    for game in order.shoppingCart:
        game = game[1:]
        games += "'" + game + "',"
    games = games[:-1]
    games += ")"
    print(order.shoppingCart)
    order.games_query = Game.objects.raw("select * from games_game where name in " + games)
    print({order.games_query})
    return render(request, 'user_shopping_cart.html', {'games': order.games_query})


def buy(request):
    if order.logged_user == "":
        return render(request, "admin_or_user.html")

    print(order.logged_user)
    user = User.objects.get(username=order.logged_user)
    games = []
    for names in order.shoppingCart:
        names = names[1:]
        games.append(Game.objects.get(name=names))
    for game in games:
        my_order = Order(client=user, game=game, date=datetime.now())
        my_order.save()
    return render(request, 'user_home_page.html')


def add_new_company(request):
    if order.logged_user == "":
        return render(request, "admin_or_user.html")

    name = request.POST.get("name", "")
    number_of_employees = int(request.POST.get("number_of_employees", ""))
    centre = request.POST.get("centre", "")

    if name and number_of_employees and centre:
        company = Company(name=name, number_of_employees=number_of_employees, centre=centre)
        company.save()
    return render(request, "admin_add_company.html")


def add_new_game(request):
    if order.logged_user == "":
        return render(request, "admin_or_user.html")

    game_name = request.POST.get("name", "")
    company_name = request.POST.get("company", "")
    summary = request.POST.get("summary", "")
    genre = request.POST.get("genre", "")
    date_of_release = request.POST.get("date_of_release", "")

    if game_name and company_name and summary and genre:
        # company = Company.from_db(db=Company, field_names="name", values=company_name)
        # company = Company.objects.raw("select * from games_company where name = " + company_name)
        # print({{company}})
        game = Game(name=game_name, company=company_name, summary=summary, genre=genre, date_of_release=date_of_release)
        game.save()
    return render(request, "admin_add_game.html")


@csrf_exempt
def delete_game(request):
    if order.logged_user == "":
        return render(request, "admin_or_user.html")

    game_id = request.POST['id']
    Game.objects.filter(id=game_id).delete()
    return render(RequestContext(request), "admin_delete_game.html")


def admin_add_company(request):
    if order.logged_user == "":
        return render(request, "admin_or_user.html")

    return render(request, "admin_add_company.html")


def admin_add_game(request):
    if order.logged_user == "":
        return render(request, "admin_or_user.html")

    return render(request, "admin_add_game.html")


def admin_delete_game(request):
    if order.logged_user == "":
        return render(request, "admin_or_user.html")

    all_games = Game.objects.raw("select * from games_game")
    return render(request, "admin_delete_game.html", {'games': all_games})


def check_user_login(request):
    username = request.POST.get("username", "")
    password = request.POST.get("password", "")
    if username and password:
        user = User.objects.raw("select * from games_user where username=" + "'" + username + "'"
                                + " and password=" + "'" + password + "'")
        if user:
            order.logged_user = username
            return render(request, "user_home_page.html")
        else:
            return render(request, "user_login.html")


def user_login(request):
    return render(request, "user_login.html")


def user_new_account(request):
    return render(request, "user_new_account.html")


def create_new_account(request):
    first_name = request.POST.get("first_name", "")
    last_name = request.POST.get("last_name", "")
    date_of_birth = request.POST.get("date_of_birth", "")
    username = request.POST.get("username", "")
    password = request.POST.get("password", "")
    if first_name and last_name and date_of_birth and username and password:
        order.logged_user = username = username
        user = User(first_name=first_name, last_name=last_name, date_of_birth=date_of_birth, username=username, password=password)
        print(user)
        user.save()
    return render(request, "user_home_page.html")
