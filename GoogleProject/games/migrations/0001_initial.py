# Generated by Django 3.0.2 on 2020-01-17 09:43

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('number_of_employees', models.IntegerField()),
                ('centre', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('summary', models.TextField(help_text='Enter a brief description of the game', max_length=1000)),
                ('genre', models.CharField(help_text='elect a genre for this game', max_length=200)),
                ('date_of_release', models.DateField(blank=True, null=True)),
                ('company', models.ForeignKey(on_delete=models.SET('Unknown'), to='games.Company')),
            ],
        ),
    ]
