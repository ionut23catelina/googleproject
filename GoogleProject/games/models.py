from django.db import models

# Create your models here.


class Game(models.Model):
    name = models.CharField(max_length=200)
    price = models.IntegerField(null=False, default=100)
    company = models.CharField(max_length=1000)
    summary = models.TextField(max_length=1000, help_text="Enter a brief description of the game")
    genre = models.CharField(max_length=200, help_text="elect a genre for this game")
    date_of_release = models.DateField(blank=True, null=True)


class Company(models.Model):
    name = models.CharField(max_length=200)
    number_of_employees = models.IntegerField(null=False)
    centre = models.CharField(max_length=200)


class User(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    date_of_birth = models.DateField(blank=False, null=False)
    username = models.CharField(max_length=200, unique=True, null=False)
    password = models.CharField(max_length=200, unique=True, null=False)


class Review(models.Model):
    comment = models.TextField(max_length=1000)
    user = models.ForeignKey("User", on_delete=models.SET(None), null=False)
    game = models.ForeignKey("Game", on_delete=models.SET(None), null=False)
    number_of_stars = models.IntegerField(null=True)


class Order(models.Model):
    client = models.ForeignKey("User", on_delete=models.SET(None), null=False)
    game = models.ForeignKey("Game", on_delete=models.SET(None), null=False)
    date = models.DateField(null=False)
