from django.urls import path

from . import views

urlpatterns = [
    path('', views.admin_or_user, name='admin_or_user'),
    path('home_page/', views.home_page, name='home_page'),
    path('index/', views.index, name='index'),
    path('admin_add_company/', views.admin_add_company, name='admin_add_company'),
    path('add_new_company/', views.add_new_company, name='add_new_company'),

    path('admin_add_game/', views.admin_add_game, name='admin_add_game'),
    path('add_new_game/', views.add_new_game, name='add_new_game'),

    path('admin_delete_game/', views.admin_delete_game, name='admin_delete_game'),
    path('delete_game/', views.delete_game, name='delete_game'),

    path('user_login/', views.user_login, name='user_login'),
    path('check_user_login/', views.check_user_login, name='check_user_login'),

    path('create_new_account/', views.create_new_account, name='create_new_account'),
    path('user_new_account/', views.user_new_account, name='user_new_account'),

    path('add_to_cart/', views.add_to_cart, name='add_to_cart'),
    path('shopping_cart/', views.shopping_cart, name='shopping_cart'),
    path('buy/', views.buy, name='buy'),

    path('game_details/', views.game_details, name='game_details'),
    path('add_review/', views.add_review, name='add_review')
]
